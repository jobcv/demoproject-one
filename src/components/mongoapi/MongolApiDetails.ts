class MongoApiDetails extends HTMLElement {
    constructor() {
        super();

    }
    connectedCallback() {
    
        const startBtn = <HTMLButtonElement>document.getElementById('start');
        startBtn?.addEventListener('click', (e: Event) => getEmployee())

        const cleanBtn = <HTMLButtonElement>document.getElementById('clean');

        cleanBtn.addEventListener('click', (e: Event) => cleanEmployee()) 


    }

}

async function getEmployee() {
    const response = await fetch("https://mongoapi.demoproject.one/nosql/api");
    const results = await response.json();
    console.log(results);
    const tbody = document.getElementsByTagName("tbody")[0];

    console.log(tbody);

    results.forEach((result: any) => {

        const tr = document.createElement("tr") as HTMLTableRowElement;
        let td = document.createElement("td") as HTMLTableCellElement;
        td.textContent = result.Employeeid
        tr.append(td)
        // tbody.append(tr)
        td = document.createElement('td') as HTMLTableCellElement
        td.textContent = result.EmployeeName
        tr.append(td)

        td = document.createElement('td') as HTMLTableCellElement
        td.textContent = result.EmployeePhone
        tr.append(td)

        tbody.append(tr)

    })


}

function cleanEmployee() {
    const tbody = document.getElementsByTagName("table")[0];
    for(var i = 1;i<tbody.rows.length;){
        tbody.deleteRow(i);
    }

}


customElements.define("mongoapidetails-mongoapidetails", MongoApiDetails);