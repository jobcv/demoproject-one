class PsqlApiDetails extends HTMLElement {
    constructor() {
        super();

    }
    connectedCallback() {
    
        const startBtn = <HTMLButtonElement>document.getElementById('start');
        startBtn?.addEventListener('click', (e: Event) => getPassengers())

        const cleanBtn = <HTMLButtonElement>document.getElementById('clean');

        cleanBtn.addEventListener('click', (e: Event) => cleanResult()) 


    }

}

async function getPassengers() {
    const response = await fetch("https://psqlapi.demoproject.one/api");
    const results = await response.json();
    console.log(results);
    const tbody = document.getElementsByTagName("tbody")[0];

    console.log(tbody);

    results.forEach((result: any) => {

        const tr = document.createElement("tr") as HTMLTableRowElement;
        let td = document.createElement("td") as HTMLTableCellElement;
        td.textContent = result.Name
        tr.append(td)
        // tbody.append(tr)
        td = document.createElement('td') as HTMLTableCellElement
        td.textContent = result.Email
        tr.append(td)

        td = document.createElement('td') as HTMLTableCellElement
        td.textContent = result.Travel_to
        tr.append(td)

        tbody.append(tr)

    })


}

function cleanResult() {
    const tbody = document.getElementsByTagName("table")[0];
    for(var i = 1;i<tbody.rows.length;){
        tbody.deleteRow(i);
    }

}


customElements.define("psqlapidetails-psqlapidetails", PsqlApiDetails);