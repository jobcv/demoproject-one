class WebsokerserviceDetails extends HTMLElement {

    constructor() {
        super();

    }
    connectedCallback() {

        let ws = new WebSocket('wss://websocket-template.elisasipvip4396.workers.dev/ws')

        const startBtn = <HTMLButtonElement>document.getElementById('start');
        startBtn?.addEventListener('click', (e: Event) => startWS(ws))

        const cleanBtn = <HTMLButtonElement>document.getElementById('clean');

        cleanBtn.addEventListener('click', (e: Event) => cleanConnection(ws))


    }

    disconnectedCallback() {
        console.log("Custom element removed from page.");
    }

    adoptedCallback() {
        console.log("Custom element moved to new page.");
    }

}

async function startWS(ws: any) {
    const tbody = document.getElementsByTagName("tbody")[0];


    console.log(ws.readyState)
    ws.send("CLICK")

    if (ws.readyState > 1) {

        console.log("readyState", ws.readyState)
    }

    ws.onmessage = function (event: any) {

        const tr = document.createElement("tr") as HTMLTableRowElement;
        let td = document.createElement("td") as HTMLTableCellElement;
        td.textContent = event.data
        tr.append(td)
        tbody.append(tr)


    }


}

function cleanConnection(ws: WebSocket) {
    const tbody = document.getElementsByTagName("table")[0];
    for (var i = 1; i < tbody.rows.length;) {
        tbody.deleteRow(i);
    }
    location.reload()
    // ws.close()

}


customElements.define("websoketservicedetails-websoketservicedetails", WebsokerserviceDetails);