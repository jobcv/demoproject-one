class AdvansedApiDetails extends HTMLElement {
    constructor() {
        super();

    }
    connectedCallback() {

        const startBtn = <HTMLButtonElement>document.getElementById('start');
        startBtn?.addEventListener('click', (e: Event) => getOrders())

        const cleanBtn = <HTMLButtonElement>document.getElementById('clean');

        cleanBtn.addEventListener('click', (e: Event) => cleanOrdersResult())


    }

}

async function getOrders() {
    const response = await fetch("https://advancedapi.demoproject.one/v1/orders");
    const results = await response.json();
    console.log(results);
    const tbody = document.getElementsByTagName("tbody")[0];

    results.data.forEach((result: any) => {

        const tr = document.createElement("tr") as HTMLTableRowElement;
        let td = document.createElement("td") as HTMLTableCellElement;
        td.className ="border px-8 py-4"
        td.textContent = result.OrderId
        tr.append(td)
        // tbody.append(tr)
        td = document.createElement('td') as HTMLTableCellElement
        td.className ="border px-8 py-4"
        td.textContent = result.Title
        tr.append(td)

        td = document.createElement('td') as HTMLTableCellElement
        td.className ="border px-8 py-4"
        td.textContent = result.CreatedAt
        tr.append(td)

        tbody.append(tr)

    })


}

function cleanOrdersResult() {
    const tbody = document.getElementsByTagName("table")[0];
    for (var i = 1; i < tbody.rows.length;) {
        tbody.deleteRow(i);
    }

}


customElements.define("advansedapidetails-advancedapidetails", AdvansedApiDetails);