class GrpcGatewayDetails extends HTMLElement {
    constructor() {
        super();

    }
    connectedCallback() {
    
        const startBtn = <HTMLButtonElement>document.getElementById('start');
        startBtn?.addEventListener('click', (e: Event) => getAdd())

        const cleanBtn = <HTMLButtonElement>document.getElementById('clean');

        cleanBtn.addEventListener('click', (e: Event) => cleanAdd()) 


    }

}

async function getAdd() {
    const response = await fetch("https://grpc.demoproject.one/grpc/api/1/3");
    const results = await response.json();
    console.log(results);
    const tbody = document.getElementsByTagName("tbody")[0];


    // results.forEach((result: any) => {

        const tr = document.createElement("tr") as HTMLTableRowElement;
        let td = document.createElement("td") as HTMLTableCellElement;
        td.textContent = results
        tr.append(td)
        tbody.append(tr)

    // })


}

function cleanAdd() {
    const tbody = document.getElementsByTagName("table")[0];
    for(var i = 1;i<tbody.rows.length;){
        tbody.deleteRow(i);
    }

}


customElements.define("grpcgatewaydetails-grpcgatewaydetails", GrpcGatewayDetails);