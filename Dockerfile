# syntax=docker/dockerfile:1

# FROM --platform=linux/arm64 node
# FROM --platform=linux/arm64 node
# FROM node
# FROM arm64v8/node
FROM node
WORKDIR /app

COPY . .
RUN rm -rf ./node_modules

RUN npm i -g pnpm
RUN pnpm install --frozen-lockfile
RUN pnpm build

ENV HOST=0.0.0.0
ENV PORT=3000
EXPOSE 3000
CMD node ./dist/server/entry.mjs

# FROM node:latest AS base

# RUN npm i -g pnpm

# FROM base AS dependencies

# WORKDIR /app
# COPY package.json pnpm-lock.yaml ./
# RUN pnpm install

# FROM base AS build

# WORKDIR /app
# COPY . .
# COPY --from=dependencies /app/node_modules ./node_modules
# RUN pnpm build
# RUN ls -trl ./

# FROM base AS deploy

# WORKDIR /app
# COPY --from=build /app/dist/ ./dist/
# COPY --from=build /app/node_modules ./node_modules
# COPY --from=build /app/dist/server/entry.mjs ./dist/server/entry.mjs
# RUN ls -trl ./dist

# CMD [ "node", "./dist/server/entry.mjs" ]

